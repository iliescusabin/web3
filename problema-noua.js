/*
Reverse and Add

Problem: choose a number, reverse its digits and add it to the original.
If the sum is not a palindrome (which means, it is not the same number from left to right and right to left), repeat this procedure. eg.

195 (initial number) + 591 (reverse of initial number) = 786

786 + 687 = 1473

1473 + 3741 = 5214

5214 + 4125 = 9339 (palindrome)

In this particular case the palindrome 9339 appeared after the 4th addition. This method leads to palindromes in a few step for almost all of the integers.

Write a function called reverseAndAdd that will take an integer as an input and return a string formatted as number of additions + palindrome number found. In the above example, reverseAndAdd(195) should produce the string “4 9339”, with “4" being the number of tries and “9339” the palindrome number.

Please do not use already existing JavaScript methods to reverse the string but build your own function.

Tips:

Write as many functions and tests as you deemed necessary to solve this task. Please focus on code quality and not only hacking something together to make it work. You will be asked to comment your solution in person.

It would also help us if you could use this screen for the entire task, so that we can see how you got to the solution of this problem.

*/


// inca una

// Se da o matrice ca cea de mai jos. Sa se construiasca o functie care ia ca parametru un cuvant - String si verifica daca exista in matrice, fie orizontal, fie vertical:
// [
// [t, w, e, r, t, y, u, I, o, p],
// [a, s, d, f, g, h, j, k, e, z],
// [c, v, b, n, o, o, f, m, l, p], 
// [l, k, j, h, f,  d,  s,  a,  d, f] 
// …
// ]
// Exemplu: 
// foo -> returneaza true
// cat -> returneaza true
