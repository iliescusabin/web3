import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import { Joke } from './models/joke.interface';

const JOKES_API: string = 'http://localhost:3000/jokes';

export class CrudJokesService {
  constructor(private http: HttpClient) {}

  getJokes(): Observable<Joke[]> {
    return this.http.get<Joke[]>(JOKES_API);
  }

  deleteJoke(joke: Joke): Observable<Joke> {
    return this.http.delete<Joke>(`${JOKES_API}/${joke.id}`);
  }

  editJoke(joke: Joke): Observable<Joke> {
    return this.http.put<Joke>(`${JOKES_API}/${joke.id}`, joke);
  }
}
