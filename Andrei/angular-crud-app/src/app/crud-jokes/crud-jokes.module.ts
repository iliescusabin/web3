import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {MatIconModule} from '@angular/material/icon';

// Containers
import { CrudJokesComponent } from './containers/crud-jokes/crud-jokes.component';

// Components
import { JokeDetailComponent } from './components/joke-detail/joke-detail.component';
import { JokeDetailEditComponent } from './components/joke-detail-edit/joke-detail-edit.component';
import { SearchJokeComponent } from './components/search-joke/search-joke.component';

// Service
import { CrudJokesService } from './crud-jokes.service';



@NgModule({
  declarations: [
    CrudJokesComponent,
    JokeDetailComponent,
    JokeDetailEditComponent,
    SearchJokeComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatButtonModule,
    MatInputModule,
    MatIconModule
  ],
  exports: [
    CrudJokesComponent
  ],
  providers: [
    CrudJokesService
  ]
})

export class CrudJokesModule {}
