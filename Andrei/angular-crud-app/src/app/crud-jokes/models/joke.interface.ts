export interface Joke {
  id: number;
  title: string;
  titleP1: string;
  titleP2: string;
  titleP3: string;
  contentP1: string;
  contentP2: string;
  contentP3: string;
  content: string;
  isEdited: boolean;
  search: boolean;
}
