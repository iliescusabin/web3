import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrudJokesComponent } from './crud-jokes.component';

describe('CrudJokesComponent', () => {
  let component: CrudJokesComponent;
  let fixture: ComponentFixture<CrudJokesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrudJokesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrudJokesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
