import { Component, OnInit, OnChanges } from '@angular/core';

import { CrudJokesService } from '../../crud-jokes.service';

import { Joke } from '../../models/joke.interface';

@Component({
  selector: 'app-crud-jokes',
  templateUrl: './crud-jokes.component.html',
  styleUrls: ['./crud-jokes.component.scss'],
})
export class CrudJokesComponent implements OnInit {
  jokes: Joke[];

  constructor(private jokesService: CrudJokesService) {}

  showJokes() {
    this.jokesService.getJokes().subscribe((data: Joke[]) => {
      this.jokes = data;
    });
  }

  ngOnInit() {
    this.showJokes();
  }

  editJoke(event: Joke) {
    event.isEdited = !event.isEdited;
  }

  deleteJoke(event: Joke) {
    this.jokesService.deleteJoke(event).subscribe((data: Joke) => {
      this.jokes = this.jokes.filter((joke: Joke) => {
        return joke.id != event.id;
      });
    });
  }

  handleCancel(event: Joke) {
    event.isEdited = !event.isEdited;
  }

  handleEdit(event: Joke) {
    this.jokesService.editJoke(event).subscribe();
    event.isEdited = !event.isEdited;
  }

  // searchJoke(event: string) {
  //   if (event.length > 3) {
  //     this.jokes = this.jokes.filter((data: Joke) => {
  //       let title = data.title.toLowerCase().indexOf(event.toLowerCase());
  //       let content = data.content.toLowerCase().indexOf(event.toLowerCase());
  //       return title !== -1 || content !== -1;
  //     });
  //   } else {
  //     this.showJokes();
  //   }
  // }

  searchActive: boolean = false;

  titleFormat(data: Joke, event: string) {
    data.titleP1 = data.title.substring(0, data.title.indexOf(event));
    data.titleP1.length === 0
      ? (data.titleP2 =
          event.substring(0, 1).toUpperCase() + event.substring(1))
      : (data.titleP2 = event);
    data.titleP3 = data.title.substring(data.titleP1.length + event.length);
  }

  contentFormat(data: Joke, event: string) {
    data.contentP1 = data.content.substring(0, data.content.indexOf(event));
    data.contentP1.length === 0
      ? (data.contentP2 =
          event.substring(0, 1).toUpperCase() + event.substring(1))
      : (data.contentP2 = event);
    data.contentP3 = data.content.substring(
      data.contentP1.length + data.contentP2.length
    );
  }

  searchJoke(event: string) {
    if (event.length > 3) {
      this.searchActive = true;
      this.jokes.map((data: Joke) => {
        let title = data.title.toLowerCase().indexOf(event.toLowerCase());
        let content = data.content.toLowerCase().indexOf(event.toLowerCase());
        if (title !== -1 || content !== -1) {
          data.search = true;

          if (title !== -1) {
            this.titleFormat(data, event);
            data.contentP1 = data.content;
          }

          if (content !== -1) {
            this.contentFormat(data, event);
            data.titleP1 = data.title;
          }

          if (title !== -1 && content !== -1) {
            this.titleFormat(data, event);
            this.contentFormat(data, event);
          }
          
        } else {
          data.search = false;
        }
      });
    } else {
      this.showJokes();
      this.searchActive = false;
    }
  }
}
