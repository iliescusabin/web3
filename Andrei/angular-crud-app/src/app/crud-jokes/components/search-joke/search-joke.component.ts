import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-search-joke',
  templateUrl: './search-joke.component.html',
  styleUrls: ['./search-joke.component.scss']
})
export class SearchJokeComponent implements OnInit {

  value: string = '';

  @Output()
  search: EventEmitter<string> = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
  }

  searchAction() {
    this.search.emit(this.value);
  }

}
