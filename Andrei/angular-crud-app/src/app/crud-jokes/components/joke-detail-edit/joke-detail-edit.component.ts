import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Joke } from '../../models/joke.interface';

@Component({
  selector: 'app-joke-detail-edit',
  templateUrl: './joke-detail-edit.component.html',
  styleUrls: ['./joke-detail-edit.component.scss']
})
export class JokeDetailEditComponent implements OnInit {

  @Input()
  joke: Joke;

  @Output()
  cancelJoke: EventEmitter<Joke> = new EventEmitter<Joke>();

  @Output()
  editJoke: EventEmitter<Joke> = new EventEmitter<Joke>();

  constructor() { }

  ngOnInit() {
  }

  cancelEdit(joke: Joke) {
    this.cancelJoke.emit(joke);
  }

  submit(title: string, content: string, joke: Joke) {
    this.joke.title = title;
    this.joke.content = content;
    this.editJoke.emit(joke);
  }
}
