import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JokeDetailEditComponent } from './joke-detail-edit.component';

describe('JokeDetailEditComponent', () => {
  let component: JokeDetailEditComponent;
  let fixture: ComponentFixture<JokeDetailEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JokeDetailEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JokeDetailEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
