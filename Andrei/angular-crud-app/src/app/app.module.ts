import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { CrudJokesModule } from './crud-jokes/crud-jokes.module';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    CrudJokesModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
