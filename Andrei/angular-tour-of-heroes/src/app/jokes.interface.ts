export interface Joke {
  id: number,
  title: string,
  content: string
}
