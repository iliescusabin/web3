import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Joke } from '../jokes.interface';

import { HeroService } from '../hero.service';

@Component({
  selector: 'app-input-joke',
  templateUrl: './input-joke.component.html',
  styleUrls: ['./input-joke.component.scss']
})
export class InputJokeComponent implements OnInit {

  isEdit: boolean = false;

  @Input()
  joke: Joke;

  @Output()
  deleteJoke: EventEmitter<Joke> = new EventEmitter<Joke>();

  @Output()
  editJoke: EventEmitter<Joke> = new EventEmitter<Joke>();

  constructor() { }

  ngOnInit() {
  }

  deleteAction(joke: Joke) {
    this.deleteJoke.emit(joke);
  }

  editAction(joke: Joke) {
    this.isEdit = true;
  }

  cancelForm() {
    this.isEdit = false;
  }

  submitForm(title: string, content: string, joke: Joke) {
    joke.title = title;
    joke.content = content;
    this.editJoke.emit(joke);
  }
}
