//1) scrieti o functie care converteste data primita ca si parametru M/D/YYYY intru-un format YYYYMMDD;
//  ex: "12/2/2019" --> 20191202.

function dateConvertor(date) {
  let year = date.substring(date.lastIndexOf("/") + 1);
  let month = parseInt(date.substring(0, date.indexOf('/')));
  let day = parseInt(
    date.substring(date.indexOf("/") + 1, date.lastIndexOf("/"))
  );
  month < 10 ? (month = "0" + month) : null;
  day < 10 ? (day = "0" + day) : null;
  return year + month + day;
}

//2) Creati o functie care elimina primul si ultimul element din array. Returneaza array-ul.
//   Daca array-ul are valori dublate returneaza array-ul cu elementele unice.

function arrayModify(array) {
  array.splice(array.length - 1, 1);
  array.splice(0, 1);
  for (let index = 0; index < array.length; index++) {
    if (array.indexOf(array[index]) == array.lastIndexOf(array[index])) {
      null;
    } else {
      array.splice(index, 1);
      index -= 1;
    }
  }
  return array;
}


// Problema 3

function reverseNumber(number) {
  number = number.toString();
  let numberReverse = [];
  for (let i = 0; i < number.length; i++) {
    numberReverse.unshift(number[i]);
  }
  numberReverse = numberReverse.join("");
  return numberReverse;
}

function palindrom(number) {
  let result = false;
  let numberReverse = reverseNumber(number);
  number == numberReverse 
  ? (result = true) 
  : (result = false);
  return result;
}

function NumberPlusReverseNumber(number) {
  let numberReverse = reverseNumber(number);
  let sum = Number(number) + Number(numberReverse);
  console.log( Number(number) + " + " + Number(numberReverse) + " = " + sum + " --> " + palindrom(sum) );
  return [palindrom(sum), sum];
}

function reverseAndAdd(number) {
  let contor = 1;
  let checkRepetition = NumberPlusReverseNumber(number);
  while (checkRepetition[0] == false) {
    contor ++;
    checkRepetition = NumberPlusReverseNumber(checkRepetition[1]);
  }
  return contor + " " + checkRepetition[1];
}

// Problema 4 

const matrix = [
  ["t", "w", "e", "r", "t", "y", "u", "I", "o", "p"],
  ["a", "s", "d", "f", "g", "h", "j", "k", "e", "z"],
  ["c", "v", "b", "n", "o", "o", "f", "m", "l", "p"],
  ["l", "k", "j", "h", "f", "d", "s", "a", "d", "f"]
];

function searchString(string) {
  let workingMatrix = [];
  let element = "";
  let reverseString = reverseNumber(string);
  let problemResult = false;
  for (let i = 0; i < matrix.length; i++) {
    workingMatrix.push(matrix[i].join(""));
  }
  for (let i = 0; i < matrix[0].length; i++) {
    for (let k = 0; k < matrix.length; k++) {
      element += matrix[k][i];
      if (k == matrix.length - 1) {
        workingMatrix.push(element);
        element = "";
      }
    }
  }
  for (let i = 0; i < workingMatrix.length; i++) {
    workingMatrix[i].indexOf(string) != -1 ||
    workingMatrix[i].indexOf(reverseString) != -1
      ? (problemResult = true)
      : null;
  }
  return problemResult;
}