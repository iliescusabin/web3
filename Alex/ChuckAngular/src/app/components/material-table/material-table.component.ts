import {Component, OnInit} from '@angular/core';

import { Person } from '../../models/Person';
import { SendDataToTableService } from '../../services/send-data-to-table.service'



/**
 * @title Basic use of `<table mat-table>`
 */

@Component({
  selector: 'app-material-table',
  templateUrl: './material-table.component.html',
  styleUrls: ['./material-table.component.css']
})
export class MaterialTableComponent implements OnInit {
  displayedColumns: string[] = ['id', 'name', 'Lastname', 'address', 'date'];
  dataSource: Person[] = [];
  constructor(
    private sendDataToTableService: SendDataToTableService
  ) { }

  ngOnInit() {
    this.sendDataToTableService.actualDataFromForm.subscribe( person => { 
      console.log(person);
      this.dataSource.push(person);
    })
  }

}
