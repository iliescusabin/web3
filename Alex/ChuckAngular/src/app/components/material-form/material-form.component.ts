import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

import { Person } from '../../models/Person';
import { SendDataToTableService } from '../../services/send-data-to-table.service'

@Component({
  selector: 'app-material-form',
  templateUrl: './material-form.component.html',
  styleUrls: ['./material-form.component.css']
})
export class MaterialFormComponent implements OnInit {
  currentPerson: Person = {
    id: null,
    name: '',
    lastName: '',
    address: '',
    date: ''
  }
  isLinear = false;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;

  constructor(
    private _formBuilder: FormBuilder,
    private sendDataToTableService: SendDataToTableService,
    private router: Router
  ) { }

  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({
      name: ['', Validators.required],
      lastName: ['', Validators.required]

    });
    this.secondFormGroup = this._formBuilder.group({
      address: ['', Validators.required],
      date: ['', Validators.required]
    });
  }

  addToTable() {
    this.currentPerson = {
      id: Math.random(),
      name: this.firstFormGroup.value.name,
      lastName: this.firstFormGroup.value.lastName,
      address: this.secondFormGroup.value.address,
      date: this.secondFormGroup.value.date
    }
    console.log(`currentPerson: ${this.currentPerson}`)
    this.sendDataToTableService.sendData(this.currentPerson);
    this.router.navigate(['/table']);

  }
}
