import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

import { Person } from '../models/Person';

@Injectable({
  providedIn: 'root'
})
export class SendDataToTableService {
  private dataFromFormular = new BehaviorSubject({
    id: null,
    name: '',
    lastName: '',
    address: '',
    date: ''
  });
  actualDataFromForm = this.dataFromFormular.asObservable();

  constructor() { }


  sendData(person: Person) { 
    return this.dataFromFormular.next(person);
  } 

}
