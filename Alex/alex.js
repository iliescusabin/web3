// 1) scrieti o functie care converteste data primita ca si parametru M-D-YYYY intr-un format YYYYMMDD; ex: 9-1-1920 in 19200109

function formatChanger(arg = '9-2-2019') { //data de tip M-D-YYYY

    const newArr = arg.split('-');

    newArr.forEach((value, index) => {
        if (value < 10) {
            let newvalue = '0' + String(value);

            newArr.splice(index, 1, newvalue);
        }

    });

    const finalArr = newArr[2] + newArr[0] + newArr[1];

    console.log(newArr);
    console.log(finalArr);

}

// formatChanger();

//2) Creati o functie care elimina prima si ultima valoroare dintr-un array si returneaza noul array si daca arrayul are valori dublate, returneaza array-ul cu elementele unice

function arrayTrimmer(arr = [12, 20, 50, 70, 50, 8]) {

    //delete first and last items
    arr.forEach((value, index) => {
        if (index == 0) {
            arr.splice(0, 1);
        };
        if (index == arr.length - 1) {
            arr.splice(index, 1);
        };

    });

    //delete duplicate item     
    arr.sort((x, y) => { return x - y });
    arr.forEach((value, index) => {
        if (arr[index] === arr[index + 1]) {
            arr.splice(index, 1);
        };

    });

    console.log(arr);

};

// arrayTrimmer();

// ultima problema 

//reverses the first number
function getReversedNumber(x) {

    let y = '',
        arr = [];

    while (x !== 0) {
        let lastNumber = x % 10;
        arr.push(lastNumber);
        x = Math.floor(x / 10);
    };

    for (let i = 0; i < arr.length; i++) {
        y += arr[i];
    };

    return parseInt(y);
}

//checks if a number is a palindorm
function palindormChecker(input) {
    let arr = [],
        dummyNumber = input,
        newNum = '';

    while (dummyNumber !== 0) {
        let lastNumber = dummyNumber % 10;
        arr.push(lastNumber);
        dummyNumber = Math.floor(dummyNumber / 10);
    };

    for (let i = 0; i < arr.length; i++) {
        newNum += arr[i];
    };

    return newNum == input;

}

function findPalindorm(x = 3698) {

    let y = getReversedNumber(x),
        sum,
        index = 0;

    do {
        sum = x + y;
        x = sum;
        y = getReversedNumber(x);
        index++;
    }
    while (palindormChecker(sum) !== true);

    return sum;

};

// console.log(findPalindorm());

//inca una

const mainArray = [
    ["t", "w", "e", "r", "t", "y", "u", "I", "o", "p"],
    ["a", "s", "d", "f", "g", "h", "j", "k", "e", "z"],
    ["c", "v", "b", "n", "o", "o", "f", "m", "l", "p"],
    ["l", "k", "j", "h", "f", "d", "s", "a", "d", "f"]
];

//merges the arrays into a single array with array method JOIN
function joinedArray(dummyArr = []) {
    let newArr = [];
    newArr = dummyArr.join(',')

    return newArr;
};

//merges the arrays into a single array by iterating
function joinedArrayTwo(dummyArr = []) {
    let newArr = [];

    for (let i = 0; i < dummyArr.length; i++) {
        for (let j = 0; j < dummyArr[i].length; j++) {
            newArr.push(dummyArr[i][j]);
        }
    }

    return newArr;
};


function searchInput(input) {
    // toggle joinedArray/joinedArrayTwo for the same result
    const arr = joinedArray(mainArray);
    let resultArr = [],
        result;

    //checks if the input is a string else it won't run
    if (typeof input === 'string') {
        //checks if the letters are in the merged array and returns a new arr with boolean values

        //solution 1 with includes method
        for (let i = 0; i < input.length; i++) {
            (arr.includes(input[i])) ? resultArr.push(true): resultArr.push(false);

            //solution 2 with index of method
            // (arr.indexOf(input[i]) !== -1) ? resultArr.push(true): resultArr.push(false);
        }
    } else {
        console.log('add a string parameter');
    }
    // if the boolean array contains at least 1 false value, then the letter is not in the merged array

    // solution 1 with includs method
    (resultArr.includes(false)) ? result = false: result = true;

    // solution 2 with index of method
    // (resultArr.indexOf(false) !== -1) ? result = false: result = true;

    return result;
};

console.log(searchInput('xtwe'));
console.log(searchInput('adf'));
console.log(searchInput('ooxf'));