import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { Joke } from "./Interfaces/joke";

const JOKES: string = "http://localhost:3000/jokes";

@Injectable({
  providedIn: "root"
})
export class ChuckNorrisService {
  constructor(private http: HttpClient) {}

  getJokes(): Observable<Joke[]> {
    return this.http.get<Joke[]>(JOKES);
  }

  deleteJoke(id:number): Observable<Joke> {
    return this.http.delete<Joke>(`${JOKES}/${id}`)
  }

  editJoke(joke: Joke): Observable<Joke> {
    return this.http.put<Joke>(`${JOKES}/${joke.id}`, joke);
  }
}
