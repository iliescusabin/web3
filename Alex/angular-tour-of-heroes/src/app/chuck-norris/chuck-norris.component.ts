import { Component, OnInit } from "@angular/core";
import { ChuckNorrisService } from "../chuck-norris.service";

import { Joke } from "../Interfaces/joke";

@Component({
  selector: "app-chuck-norris",
  templateUrl: "./chuck-norris.component.html",
  styleUrls: ["./chuck-norris.component.scss"]
})
export class ChuckNorrisComponent implements OnInit {
  jokes: Joke[];

  constructor(private jokeService: ChuckNorrisService) {}

  getAllJokes() {
    this.jokeService
    .getJokes()
    .subscribe((data: Joke[]) => (this.jokes = data));
  }

  ngOnInit() {
    this.getAllJokes();
  }

  actionDelete(joke: Joke) {
    console.log(joke)
    this.jokeService.deleteJoke(joke.id).subscribe();
    this.getAllJokes();
  }

  actionEdit(joke: Joke) {
    this.jokeService.editJoke(joke).subscribe((data: Joke) => {
      this.getAllJokes();
    })
  }
}
