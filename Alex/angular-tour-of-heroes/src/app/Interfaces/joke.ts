export interface Joke {
  id: number;
  title: String;
  content: String;
}
