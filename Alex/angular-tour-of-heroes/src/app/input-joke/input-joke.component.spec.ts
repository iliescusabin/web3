import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InputJokeComponent } from './input-joke.component';

describe('InputJokeComponent', () => {
  let component: InputJokeComponent;
  let fixture: ComponentFixture<InputJokeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InputJokeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputJokeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
