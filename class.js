var Person = function(nume, prenume, varsta, anNastere) {
    this.nume = nume;
    this.prenume = prenume;
    var _varsta = varsta;
    this.anNastere = anNastere;
    this.getVarsta = function() {
        return _varsta;
    }
    this.numeComplet = function() {
        return `${this.nume} ${this.prenume}`;
    }
}

var Programator = function(nume, prenume, varsta, anNastere, experienta) {
    // Person.apply(this, [nume, prenume, varsta, anNastere]);
    Person.call(this, nume, prenume, varsta, anNastere);
    this.experienta = experienta;
}
Programator.numeComplet = function() {
    Person.numeComplet.call(this);
}

var Andrei = new Programator('Andrei', 'Golopenta', 32, '27-11-1986', 0.5);
console.log(Andrei.nume);
console.log(Andrei.prenume);
var Luiza = new Programator('Luiza','Ulm',25,'03-11-1990',5);
console.log(Luiza.nume);

console.log(Andrei.numeComplet());

Person.prototype.setAnNastere = function(newAn){
    return this.anNastere = newAn;
}
Programator.prototype.setAnNastere = function(newAn) {
    Person.prototype.setAnNastere.call(this, newAn);
    // Person.prototype.setAnNastere.apply(this, [newAn]);
}
Andrei.setAnNastere(28);
console.log(Andrei.anNastere);
var Sabin = new Person('Sabin', 'Iliescu', 37);
// console.log(Sabin.nume);
// console.log(Sabin.getVarsta());
Sabin.nume = 'Mihai';
// console.log(Sabin.nume);
Sabin.varsta = 27;
// console.log(Sabin.getVarsta());

Sabin.setAnNastere('20-11-1986');
// console.log(Sabin.anNastere);
