// Angular Maps
import { AgmCoreModule } from '@agm/core';

// Angular Material
import { MaterialModule } from './modules/material/material.module';

// Angular Modules
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

// Components
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { HomeComponent } from './components/home/home.component';
import { OurServicesComponent } from './components/our-services/our-services.component';
import { OurServicesMenuComponent } from './components/our-services/our-services-menu/our-services-menu.component';
import { ServicePresentationComponent } from './components/our-services/service-presentation/service-presentation.component';
import { ContactUsComponent } from './components/contact-us/contact-us.component';

// Font Awesome
import { AngularFontAwesomeModule } from 'angular-font-awesome';

// Services
import { MockServicesService } from './services/our-services.service';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    OurServicesComponent,
    OurServicesMenuComponent,
    ServicePresentationComponent,
    ContactUsComponent
  ],
  imports: [
    // Angular
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    // Angular Maps
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDCGEOUZxEAbMj9QwyI3fXiGytAx1aUy-c',
    }),
    // Angular Material
    MaterialModule,
    // Font Awesome
    AngularFontAwesomeModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
