import { Injectable } from '@angular/core';

import { CompanyInfo } from '../models/company-info';
import { COMPANYINFO } from '../data/mock-company-info';

@Injectable({
  providedIn: 'root'
})
export class CompanyInfoService {
  companyInfo: CompanyInfo = COMPANYINFO;

  constructor() { }

  getCompanyInfo() {
    return this.companyInfo;
  }
}
