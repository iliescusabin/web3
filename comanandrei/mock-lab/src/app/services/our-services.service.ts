import { Injectable } from '@angular/core';

import { ServiceModel } from '../models/service-model';
import { OURSERVICES } from '../data/mock-our-services';

import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MockServicesService {
  services: ServiceModel[] = OURSERVICES;

  private service = new BehaviorSubject<ServiceModel>(OURSERVICES[0]);
  selectedService = this.service.asObservable();

  constructor() { }

  getMockServices() {
    return this.services;
  }

  getServiceData(id: number) {
    // this.selectedService = this.services[id];
    // console.log(this.selectedService);
    // return this.selectedService;

    this.service.next(this.services[id]);
  }

}
