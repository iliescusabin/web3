export class ServiceModel {
  name: string;
  description: string;
  icon: string;
  iconName: string;
}
