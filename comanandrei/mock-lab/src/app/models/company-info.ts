export class CompanyInfo {
  name: string;
  adress: string;
  phone: string;
  email: string;
  website: string;
}
