export const OURSERVICES = [
  {
    name: 'Business Strategy',
    description: `Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt voluptate, quibusdam sunt iste dolores consequatur
    Inventore fugit error iure nisi reiciendis fugiat illo pariatur quam sequi quod iusto facilis officiis nobis
    sit quis molestias asperiores rem, blanditiis! Commodi exercitationem vitae deserunt qui nihil ea, tempore et quam natus quaerat
    doloremque.`,
    icon: '<fa name="briefcase" class="big-awesome-icon"></fa>',
    iconName: 'briefcase',
  },
  {
    name: 'Research',
    description: `Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt voluptate, quibusdam sunt iste dolores consequatur
    Inventore fugit error iure nisi reiciendis fugiat illo pariatur quam sequi quod iusto facilis officiis nobis
    sit quis molestias asperiores rem, blanditiis! Commodi exercitationem vitae deserunt qui nihil ea, tempore et quam natus quaerat
    doloremque.`,
    icon: '<fa name="flask" class="big-awesome-icon"></fa>',
    iconName: 'flask',
  },
  {
    name: 'Data Analysis',
    description: `Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt voluptate, quibusdam sunt iste dolores consequatur
    Inventore fugit error iure nisi reiciendis fugiat illo pariatur quam sequi quod iusto facilis officiis nobis
    sit quis molestias asperiores rem, blanditiis! Commodi exercitationem vitae deserunt qui nihil ea, tempore et quam natus quaerat
    doloremque.`,
    icon: '<fa name="edit" class="big-awesome-icon"></fa>',
    iconName: 'edit',
  },
  {
    name: 'UI Design',
    description: `Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt voluptate, quibusdam sunt iste dolores consequatur
    Inventore fugit error iure nisi reiciendis fugiat illo pariatur quam sequi quod iusto facilis officiis nobis
    sit quis molestias asperiores rem, blanditiis! Commodi exercitationem vitae deserunt qui nihil ea, tempore et quam natus quaerat
    doloremque.`,
    icon: '<fa name="table" class="big-awesome-icon"></fa>',
    iconName: 'table',
  },
  {
    name: 'Ux Design',
    description: `Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt voluptate, quibusdam sunt iste dolores consequatur
    Inventore fugit error iure nisi reiciendis fugiat illo pariatur quam sequi quod iusto facilis officiis nobis
    sit quis molestias asperiores rem, blanditiis! Commodi exercitationem vitae deserunt qui nihil ea, tempore et quam natus quaerat
    doloremque.`,
    icon: '<fa name="sitemap" class="big-awesome-icon"></fa>',
    iconName: 'sitemap',
  },
  {
    name: 'Technology',
    description: `Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt voluptate, quibusdam sunt iste dolores consequatur
    Inventore fugit error iure nisi reiciendis fugiat illo pariatur quam sequi quod iusto facilis officiis nobis
    sit quis molestias asperiores rem, blanditiis! Commodi exercitationem vitae deserunt qui nihil ea, tempore et quam natus quaerat
    doloremque.`,
    icon: '<fa name="tablet" class="big-awesome-icon"></fa>',
    iconName: 'tablet',
  },
  {
    name: 'Creative',
    description: `Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt voluptate, quibusdam sunt iste dolores consequatur
    Inventore fugit error iure nisi reiciendis fugiat illo pariatur quam sequi quod iusto facilis officiis nobis
    sit quis molestias asperiores rem, blanditiis! Commodi exercitationem vitae deserunt qui nihil ea, tempore et quam natus quaerat
    doloremque.`,
    icon: '<fa name="pencil" class="big-awesome-icon"></fa>',
    iconName: 'pencil',
  },
];
