import { Component, OnInit } from '@angular/core';

import { ServiceModel } from '../../../models/service-model';
import {  MockServicesService } from '../../../services/our-services.service';

@Component({
  selector: 'app-our-services-menu',
  templateUrl: './our-services-menu.component.html',
  styleUrls: ['./our-services-menu.component.scss']
})
export class OurServicesMenuComponent implements OnInit {
  mockServices: ServiceModel[];
  currentSelectedService: ServiceModel;

  constructor(private mockServicesService: MockServicesService) { }

  ngOnInit() {
    this.mockServices = this.mockServicesService.getMockServices();
  }

  onGetServiceData(id: number) {
    this.mockServicesService.getServiceData(id);
  }

}
