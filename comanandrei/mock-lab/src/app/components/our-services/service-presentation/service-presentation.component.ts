import { Component, OnInit } from '@angular/core';

import { ServiceModel } from '../../../models/service-model';
import {  MockServicesService } from '../../../services/our-services.service';

@Component({
  selector: 'app-service-presentation',
  templateUrl: './service-presentation.component.html',
  styleUrls: ['./service-presentation.component.scss']
})
export class ServicePresentationComponent implements OnInit {
  mockServices: ServiceModel[];
  currentSelectedService: ServiceModel;

  constructor(private mockServicesService: MockServicesService) { }

  ngOnInit() {
    this.mockServices = this.mockServicesService.getMockServices();
    this.mockServicesService.selectedService.subscribe(currentSelectedService => {
      this.currentSelectedService = currentSelectedService;
    });
    // console.log(this.currentSelectedService);
  }

}
