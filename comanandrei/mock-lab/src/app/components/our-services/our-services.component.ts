import { Component, OnInit } from '@angular/core';

import { ServiceModel } from '../../models/service-model';

@Component({
  selector: 'app-our-services',
  templateUrl: './our-services.component.html',
  styleUrls: ['./our-services.component.scss'],
  providers: []
})
export class OurServicesComponent implements OnInit {
  currentSelectedService: ServiceModel;

  constructor() { }

  ngOnInit() { }

}
