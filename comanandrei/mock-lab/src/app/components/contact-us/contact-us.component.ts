import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';

import { CompanyInfo } from '../../models/company-info';
import { CompanyInfoService } from '../../services/company-info.service';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss']
})
export class ContactUsComponent implements OnInit {
  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);
  matcher = new MyErrorStateMatcher();

  // Angular Maps code
  latitude = 39.255580;
  longitude = 60.109351;

  companyInfo: CompanyInfo;

  constructor(private companyInfoService: CompanyInfoService) { }

  ngOnInit() {
    this.companyInfo = this.companyInfoService.getCompanyInfo();
  }

}
