# ComanAndreiCvPage

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.2.1.  and uses Bootstrap 4.

In order to get it working: 

    1. Navigate to the folder from a console, use "npm install".

    2. Use "npm install --save bootstrap".

    3. In your IDE open the "angular.json" file.

    4. On line 30, in the "styles" array, add "./node_modules/bootstrap/dist/css/bootstrap.min.css" WITH the quotes!

    5. Use "ng serve" in the console to start a local server.

    6. Open up a web browser and navigate to localhost:4200
