/////////////////////////////////
// Array Exercise

const games = [
  "Warcraft",
  "Starcraft",
  "Mass Effect",
  "Dragon Age",
  "Warcraft",
  "GTA 4",
  "Guild Wars",
  "Guild Wars",
  "Mass Effect",
  "Unreal Tournament"
];

// Without Array.prototype methods

// const myShift = function(array) {
//   for (let arrayIndex = 0; arrayIndex < array.length; arrayIndex++) {
//     array[arrayIndex] = array[arrayIndex + 1];
//   }
//   array.length = array.length - 1;
//   return array;
// };

// const myPop = function(array) {
//     array.length -= 1;
//     return array;
// }

// const shiftAndPop = function(array) {
//     const newArray = [...array];
//     newArray.length -= 1;
//     for (let arrayIndex = 0; arrayIndex < newArray.length; arrayIndex++) {
//         newArray[arrayIndex] = newArray[arrayIndex + 1];
//       }
//       newArray.length -= 1;
//       return newArray;
// }

// const removeDuplicates = function(array) {
//     const newArray = [...array];
//     for(let arrayIndex = 0; arrayIndex < newArray.length; arrayIndex++) {
//         for(let arrayIndex2 = arrayIndex + 1; arrayIndex2 < newArray.length; arrayIndex2++) {
//             if (newArray[arrayIndex] === newArray[arrayIndex2]) {
//                 for (let arrayIndex3 = arrayIndex2; arrayIndex3 < newArray.length; arrayIndex3++) {
//                     newArray[arrayIndex3] = newArray[arrayIndex3 + 1];
//                 }
//                 newArray.length -=1;
//             }
//         }
//     }
//     return newArray;
// }

const removeDuplicatesThenShiftAndPop = function(array) {
  const newArray = [...array];
  for (let arrayIndex = 0; arrayIndex < newArray.length; arrayIndex++) {
    for (
      let arrayIndex2 = arrayIndex + 1;
      arrayIndex2 < newArray.length;
      arrayIndex2++
    ) {
      if (newArray[arrayIndex] === newArray[arrayIndex2]) {
        for (
          let arrayIndex3 = arrayIndex2;
          arrayIndex3 < newArray.length;
          arrayIndex3++
        ) {
          newArray[arrayIndex3] = newArray[arrayIndex3 + 1];
        }
        newArray.length -= 1;
      }
    }
  }
  newArray.length -= 1;
  for (let arrayIndex = 0; arrayIndex < newArray.length; arrayIndex++) {
    newArray[arrayIndex] = newArray[arrayIndex + 1];
  }
  newArray.length -= 1;
  return newArray;
};

// With Array.prototype methods

const removeDuplicatesShiftAndPopWithArrayMethods = function(array) {
  let newArray = [...array];
  newArray = newArray.filter(
    (value, index, newArray) => newArray.indexOf(value) === index
  );
  newArray.shift();
  newArray.pop();
  return newArray;
};

console.log(games);
console.log(removeDuplicatesThenShiftAndPop(games));
console.log(removeDuplicatesShiftAndPopWithArrayMethods(games));

/////////////////////////////////////
// Convert Date Format Exercise

let myDate = "3/1/1994";

// const convertDateFormat = (date) => date.split('/').reverse().join('/');

const convertDateFormat = date => {
  let dateHolder = date.split("/");
  for (let index = 0; index <= 2; index++) {
    if (dateHolder[index] < 10) {
      dateHolder[index] = "0" + dateHolder[index];
    }
  }
  return dateHolder.reverse().join("/");
};

////////////////////////////
// Reverse and Add

// created a function that reverses the number
const reverseNumber = number => {
  let stringifiedNumber = number.toString();
  const numberHolder = stringifiedNumber.split("");
  const newHolder = [];
  for (
    let numberHolderIndex = numberHolder.length - 1;
    numberHolderIndex >= 0;
    numberHolderIndex--
  ) {
    newHolder.push(numberHolder[numberHolderIndex]);
  }
  return parseInt(newHolder.join(""));
};

// created a function that checks if the number is already a palindrome
const checkIfPlaindrome = number => {
  if (number === reverseNumber(number)) {
    return true;
  } else {
    return false;
  }
};

// created the reverse and add function
const reverseAndAdd = number => {
  let numberOfTries = 0;
  while (checkIfPlaindrome(number) !== true) {
    counter++;
    number += reverseNumber(number);
  }
  return `${numberOfTries}, ${number}`;
};

//////////////////////////////
// Matrix exercise

const matrix = [
  ["t", "w", "e", "r", "t", "y", "u", "I", "o", "p"],
  ["a", "s", "d", "f", "g", "h", "j", "k", "e", "z"],
  ["c", "v", "b", "n", "o", "o", "f", "m", "l", "p"],
  ["l", "k", "j", "h", "f", "d", "s", "a", "d", "f"]
];


// "verticalize" matrix

const verticalizeMatrix = (matrix) => {
  const matrixRows = matrix.length;
  const matrixColumns = matrix[0].length;
  let temporaryArray = [];
  for (let index = 0; index < matrixColumns; index++) {
    for (let index2 = 0; index2 < matrixRows; index2++) {
      temporaryArray.push(matrix[index2][index]);
    }
  }
  let newMatrix = [];
  for (let index = 0; index < temporaryArray.length; index += matrix.length) {
    newMatrix.push(temporaryArray.slice(index, index + matrix.length));
  }
  return newMatrix;
};

const searchInMatrix = (myString, matrix) => {
    const verticalMatrix = verticalizeMatrix(matrix);
    let myBoolean = false;

    const searchThroughBothMatrixes = (matrix) => {
    for(let index = 0; index < matrix.length; index++) {
        const startToEnd = matrix[index].join("");
        const endToStart = matrix[index].reverse().join("");

        if (startToEnd.search(myString) > -1 || endToStart.search(myString) > -1) {
            myBoolean = true;
        }
    }
    return myBoolean;
}

    if(searchThroughBothMatrixes(matrix) || searchThroughBothMatrixes(verticalMatrix)) {
        return true;
    } else {
        return false;
    }

};
