import { Injectable } from '@angular/core';

// RXJS
import { BehaviorSubject, Observable } from 'rxjs';

//
import { Person } from './models/Person';

@Injectable({
  providedIn: 'root'
})
export class SendDataToTabeService {
  private dataFromFormular = new BehaviorSubject({
    id: null,
    name: '',
    lastName: '',
    adress: '',
    date: ''
  });

  actualDataFromForm = this.dataFromFormular.asObservable();

  constructor() { }

  sendData(person: Person) {
    return this.dataFromFormular.next(person);
  }
}
