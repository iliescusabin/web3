import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatInputModule} from '@angular/material/input';
import {MatListModule} from '@angular/material/list';
import {MatDividerModule} from '@angular/material/divider';
import {MatButtonModule} from '@angular/material/button';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatFormFieldModule} from '@angular/material/form-field';
import { Routes, RouterModule } from '@angular/router';
import {MatStepperModule} from '@angular/material/stepper';
import {MatTableModule} from '@angular/material/table';
import { ReactiveFormsModule } from '@angular/forms'

import { AppComponent } from './app.component';
import { FormComponent } from './components/form/form.component';
import { JokesComponent } from './components/jokes/jokes.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { FilterPipe } from './filter.pipe';
import { MaterialFormComponent } from './components/material-form/material-form.component';
import { MaterialTableComponent } from './components/material-table/material-table.component';


const appRoutes: Routes = [
  { path: 'form', component: MaterialFormComponent },
  { path: 'table', component: MaterialTableComponent }
];


@NgModule({
  declarations: [
    AppComponent,
    FormComponent,
    JokesComponent,
    NavbarComponent,
    FilterPipe,
    MaterialFormComponent,
    MaterialTableComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatListModule,
    MatDividerModule,
    MatButtonModule,
    MatToolbarModule,
    MatFormFieldModule,
    RouterModule.forRoot(appRoutes),
    MatStepperModule,
    MatTableModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
