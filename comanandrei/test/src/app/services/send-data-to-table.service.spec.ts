import { TestBed } from '@angular/core/testing';

import { SendDataToTableService } from './send-data-to-table.service';

describe('SendDataToTableService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SendDataToTableService = TestBed.get(SendDataToTableService);
    expect(service).toBeTruthy();
  });
});
