import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';

import { Hero } from './hero';
import { HEROES } from './mock-heroes';
import { MessageService } from './message.service';

import { HttpClient } from '@angular/common/http';

import { Joke } from './jokes.interface';

const JOKES: string = 'http://localhost:3000/jokes';

@Injectable({
  providedIn: 'root',
})
export class HeroService {

  constructor(private messageService: MessageService, private http: HttpClient) { }

  getHeroes(): Observable<Hero[]> {
    // TODO: send the message _after_ fetching the heroes
    this.messageService.add('HeroService: fetched heroes');
    return of(HEROES);
  }

  getJokes(): Observable<Joke[]> {
    return this.http.get<Joke[]>(JOKES);
  }

  deleteJoke(id:number): Observable<Joke> {
    return this.http.delete<Joke>(`${JOKES}/${id}`)
  }

  editJoke(joke: Joke): Observable<Joke> {
    return this.http.put<Joke>(`${JOKES}/${joke.id}`, joke);
  }
}
