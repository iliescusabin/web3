import { Component, OnInit } from '@angular/core';

import { HeroService } from '../hero.service';

import { Joke } from '../jokes.interface';

@Component({
  selector: 'app-chuck-norris',
  styleUrls: ['./chuck-norris.component.scss'],
  templateUrl: './chuck-norris.component.html',
})
export class ChuckNorrisComponent implements OnInit {
  jokes: Joke[];
  constructor(private jokeService: HeroService) {}

  getAllJokes() {
    this.jokeService
    .getJokes()
    .subscribe((data: Joke[]) => (this.jokes = data));
  }

  ngOnInit() {
    this.getAllJokes();
  }

  actionDelete(joke: Joke) {
    console.log(joke)
    this.jokeService.deleteJoke(joke.id).subscribe();
    this.getAllJokes();
  }

  actionEdit(joke: Joke) {
    this.jokeService.editJoke(joke).subscribe((data: Joke) => {
      this.getAllJokes();
    })
  }
}
